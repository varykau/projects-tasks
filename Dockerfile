FROM python
RUN mkdir -p /home/emerline/task_management
COPY ./task_management/requirements.txt /home/emerline/task_management
RUN pip install -r /home/emerline/task_management/requirements.txt
COPY . /home/emerline
WORKDIR /home/emerline/task_management
