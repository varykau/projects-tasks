import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'task_management.settings')


app = Celery('task_management', broker='pyamqp://guest@rabbitmq:5672//')
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

app.conf.update(
    task_serializer='json',
    result_serializer='json',
    timezone='Europe/Minsk',
    enable_utc=True,
)
