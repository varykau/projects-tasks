from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken import views as token_views

from users.views import TokenView


api_v1_patterns = [
    path('', include('tasks.urls')),
    path('users/', include('users.urls')),
    path('api-token/', TokenView.as_view(), name='token'),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(api_v1_patterns)),
]
