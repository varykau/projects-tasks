from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from .models import ROLES, User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'password', 'role', 'is_staff', 'tasks')
        read_only_fields = ('is_staff', 'tasks')

    def create(self, validated_data):
        role = validated_data.get('role', 'dev')
        user = User.objects.create_user(validated_data['email'],
                                        validated_data['password'],
                                        role)
        return user

    def update(self, instance, validated_data):
        if 'role' in validated_data and validated_data['role'] not in \
                                            list(map(lambda x: x[0], ROLES)):
            raise serializers.ValidationError(
                f'Role value must be in {list(map(lambda x: x[0], ROLES))}')
        user = super().update(instance, validated_data)
        if validated_data.get('password'):
            user.set_password(validated_data['password'])
            user.save()
        return user
