# Generated by Django 2.2 on 2019-04-23 19:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='role',
            field=models.TextField(blank=True, choices=[('Man', 'Manager'), ('Dev', 'Developer')], max_length=3),
        ),
    ]
