from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAdminUser
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.serializers import ValidationError

from .serializers import UserSerializer
from .models import User


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminUser]
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def create(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        if user:
            token = Token.objects.create(user=user)
            return Response({"token": token.key},
                            status=status.HTTP_201_CREATED)
        return Response({'status': 'User was not created'},
                        status=status.HTTP_400_BAD_REQUEST)


class TokenView(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        if not request.data.get('email', '').strip():
            raise ValidationError(
                f"Email and password must be provided")
        request.data['username'] = request.data.get('email')
        return super().post(request, *args, **kwargs)
