import json

from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from django.urls import reverse
from django.contrib.auth.hashers import make_password

from .models import User


class AccountsTest(APITestCase):
    def setUp(self):
        self.test_user = User.objects.create_user('test@example.com', 'testpassword', 'man')
        self.test_user2 = User.objects.create_user('test2@example.com', 'testpassword', 'dev')
        self.test_user3 = User.objects.create_user('test3@example.com', 'testpassword',)
        self.token = Token.objects.create(user=self.test_user)
        self.token2 = Token.objects.create(user=self.test_user2)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.users_url = reverse("users-list")

    def test_create_user_without_role(self):
        data = {
            'email': 'test@test.com',
            'password': 'testpassword'
        }
        response = self.client.post(self.users_url, data, format='json')
        token = Token.objects.order_by('created').last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertFalse('password' in response.data)
        self.assertTrue('token' in response.data)
        self.assertEqual(User.objects.count(), 4)
        self.assertEqual(User.objects.last().email, data['email'])
        self.assertEqual(User.objects.last().role, 'dev')
        self.assertEqual(User.objects.last().is_staff, False)
        self.assertEqual(token.key, response.data['token'])

    def test_create_user_with_role_man(self):
        data = {
            'email': 'test@test.com',
            'password': '123',
            'role': 'dev'
        }
        response = self.client.post(self.users_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 4)
        self.assertEqual(User.objects.last().email, data['email'])
        self.assertEqual(User.objects.last().role, 'dev')
        self.assertEqual(User.objects.last().is_staff, False)

    def test_create_user_with_role_dev(self):
        data = {
            'email': 'test@test.com',
            'password': '123',
            'role': 'man'
        }
        response = self.client.post(self.users_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 4)
        self.assertEqual(User.objects.last().email, data['email'])
        self.assertEqual(User.objects.last().role, 'man')
        self.assertEqual(User.objects.last().is_staff, True)

    def test_create_user_with_duplicate_email(self):
        data = {
            'email': 'test@example.com',
            'password': '123',
            'role': 'man'
        }
        response = self.client.post(self.users_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 3)

    def test_create_user_with_incorrect_email(self):
        data = {
            'email': 'test',
            'password': '123',
            'role': 'man'
        }
        response = self.client.post(self.users_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 3)

    def test_create_user_without_password(self):
        data = {
            'email': 'test@test.com',
            'password': ""
        }
        response = self.client.post(self.users_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 3)

    def test_create_with_unauthorized_user(self):
        data = {
            'email': 'test@test.com',
            'password': '123'
        }
        self.client.credentials()
        response = self.client.post(self.users_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(User.objects.count(), 3)

    def test_create_with_dev_user(self):
        data = {
            'email': 'test@test.com',
            'password': '123'
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token2.key)
        response = self.client.post(self.users_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(User.objects.count(), 3)

    def test_patch_user_password(self):
        data = {
            'password': '123'
        }
        update_user_url = f"{self.users_url}2/"
        response = self.client.patch(update_user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_patch_user_empty_password(self):
        data = {
            'password': ''
        }
        update_user_url = f"{self.users_url}2/"
        response = self.client.patch(update_user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_patch_user_role(self):
        data = {
            'role': ''
        }
        update_user_url = f"{self.users_url}3/"
        response = self.client.patch(update_user_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_users(self):
        response = self.client.get(self.users_url, format='json')
        data = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data[0]['email'], self.test_user.email)
        self.assertFalse('password' in data[0])
        self.assertTrue(all([field in data[0] for field in
                            ['id', 'email', 'role']]))

    def test_get_token(self):
        url = reverse('token')
        data = {
            "email": "test@example.com",
            "password": "testpassword"
        }
        response = self.client.post(url, data, format='json')
        token = Token.objects.get(user=self.test_user)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)
        self.assertTrue(token.key, response.data['token'])

    def test_get_token_empty_email(self):
        url = reverse('token')
        data = {
            "email": "",
            "password": "testpassword"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_token_empty_password(self):
        url = reverse('token')
        data = {
            "email": "test@example.com",
            "password": ""
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_token_get_request(self):
        url = reverse('token')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code,
                         status.HTTP_405_METHOD_NOT_ALLOWED)
