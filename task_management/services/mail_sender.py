from django.core.mail import EmailMessage
from django.conf import settings
from smtplib import SMTPException


def send_notification(task, email_address, period):
    remains_period = settings.PERIODS[period][0]
    email_message = EmailMessage(
            f'Notification about {task.title}',
            f'It remains {remains_period} until {task.title}.',
            to=[email_address]
            )
    try:
        email_message.send(fail_silently=False)
    except SMTPException as e:
        print(f'Message was not sent. Errors: {e}.')
