import datetime as dt
from datetime import timedelta
from django.conf import settings
from rest_framework import serializers

from users.models import User
from .models import Project, Task
from .helpers import NotificationChecker


class SaveCreatorMixin(serializers.ModelSerializer):
    creator = serializers.PrimaryKeyRelatedField(
                    queryset=User.objects.all(),
                    default=serializers.CurrentUserDefault()
                )

class ProjectSerializer(SaveCreatorMixin, serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ('id', 'title', 'description', 'staff', 'created', 'creator')


class TaskSerializer(SaveCreatorMixin, serializers.ModelSerializer):
    hour_notificated = serializers.BooleanField(read_only=True)
    day_notificated = serializers.BooleanField(read_only=True)

    class Meta:
        model = Task
        fields = ('id', 'title', 'description', 'created', 'due_date',
                  'project', 'assigned_to', 'creator',
                  'hour_notificated', 'day_notificated')

    def create(self, validated_data):
        checker = NotificationChecker(validated_data)
        validated_data = checker.full_check_notification()
        return super().create(validated_data)

    def update(self, instance, validated_data):
        checker = NotificationChecker(validated_data)
        validated_data = checker.full_check_notification()
        return super().update(instance, validated_data)

