from django.conf import settings
from django.db import models


class AbstractInfo(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL,
                                on_delete=models.SET_NULL,
                                blank=True, null=True
                                )

    class Meta:
        abstract = True


class Project(AbstractInfo):
    staff = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                   related_name="projects",
                                   blank=True
                                   )

    def __str__(self):
        return f"Project {self.title}"


class Task(AbstractInfo):
    due_date = models.DateTimeField()
    project = models.ForeignKey(Project, blank=True, null=True,
                                on_delete=models.SET_NULL)
    assigned_to = models.ForeignKey(settings.AUTH_USER_MODEL,
                                    on_delete=models.SET_NULL,
                                    blank=True, null=True,
                                    related_name="tasks"
                                    )
    hour_notificated = models.BooleanField(default=False)
    day_notificated = models.BooleanField(default=False)

    def __str__(self):
        return f"Task {self.title} of {self.project}"
