from django.conf import settings
from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.permissions import IsAdminUser
from rest_framework.status import HTTP_404_NOT_FOUND
from rest_framework.views import APIView, Response

from task_management.permissions import IsAdminOrReadOnly
from users.serializers import UserSerializer

from .models import Project, Task
from .serializers import ProjectSerializer, TaskSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminOrReadOnly]
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class TaskViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAdminOrReadOnly]
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class ProjectStaffView(APIView):

    def get(self, request, pk):
        project = Project.objects.filter(pk=pk).first()
        if not project:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serialized_users = UserSerializer(project.staff, many=True)
        return Response(serialized_users.data)

