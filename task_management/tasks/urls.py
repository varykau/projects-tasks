from rest_framework import routers
from django.urls import path
from .views import ProjectViewSet, TaskViewSet, ProjectStaffView

router = routers.SimpleRouter()
router.register('projects', ProjectViewSet, basename='projects')
router.register('tasks', TaskViewSet, basename='tasks')

urlpatterns = [
    path('projects/<pk>/staff', ProjectStaffView.as_view()),
]

urlpatterns += router.urls
