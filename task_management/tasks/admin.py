from django.contrib import admin

from .models import Task, Project


class SavedAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        obj.creator = request.user
        super().save_model(request, obj, form, change)

admin.site.register(Project, SavedAdmin)
admin.site.register(Task, SavedAdmin)
