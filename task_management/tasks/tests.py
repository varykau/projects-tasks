import json
import datetime as dt

from django.utils import timezone
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from django.urls import reverse

from .models import Task, Project
from users.models import User


class AccountsTest(APITestCase):
    def setUp(self):
        self.test_manag = User.objects.create_user('man@example.com', 'testpassword', 'man')
        self.test_manag2 = User.objects.create_user('man2@example.com', 'testpassword', 'man')
        self.test_devel = User.objects.create_user('dev@example.com', 'testpassword', 'dev')
        self.test_devel2 = User.objects.create_user('dev2@example.com', 'testpassword', 'dev')
        self.project1 = Project.objects.create(title="Project #1", description='Description of project #1',
                                               creator=self.test_manag)
        Project.objects.create(title="Project #2", description='Description of project #2',
                               creator=self.test_manag)
        self.day_after_tommorow = timezone.now() + dt.timedelta(days=2)
        self.now_plus_hour = timezone.now() + dt.timedelta(hours=1)
        self.expired_date = timezone.now() - dt.timedelta(hours=1)
        self.project1.staff.add(self.test_manag, self.test_devel, self.test_devel2)
        self.task1 = Task.objects.create(title="Task #1", 
                                         description='Description of task #1',
                                         project=self.project1,
                                         assigned_to=self.test_devel,
                                         due_date=self.day_after_tommorow)
        self.task2 = Task.objects.create(title="Task #2",
                                         description='Description of task #2',
                                         project=self.project1,
                                         due_date=self.expired_date,
                                         hour_notificated=True,
                                         day_notificated=True)
        self.token = Token.objects.create(user=self.test_manag)
        self.token_developer = Token.objects.create(user=self.test_devel)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        self.projects_url = reverse("projects-list")
        self.tasks_url = reverse("tasks-list")



    def test_get_projects(self):
        fields = ('id', 'title', 'description', 'staff', 'created', 'creator')
        response = self.client.get(self.projects_url, format='json')
        data = json.loads(response.content)
        projects_count = Project.objects.count()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(all([field in data[0] for field in fields]))
        self.assertEqual(projects_count, len(data))

    def test_post_project(self):
        data = {
            "title": "Test"
        }
        response = self.client.post(self.projects_url, data, format='json')
        count_projects = Project.objects.count()
        last_project = Project.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(count_projects, 3)
        self.assertEqual(response.data['title'], last_project.title)

    def test_post_project_empty_body(self):
        data = {
        }
        response = self.client.post(self.projects_url, data, format='json')
        count_projects = Project.objects.count()
        last_project = Project.objects.last()
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(count_projects, 2)

    def test_get_projects_no_token(self):
        self.client.credentials()
        response = self.client.get(self.projects_url, format='json')
        data = json.loads(response.content)
        task_count = Project.objects.count()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(task_count, len(data))

    def test_post_project_as_developer(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' +
                                self.token_developer.key)
        data = {
            "title": "Test"
        }
        response = self.client.post(self.projects_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_patch_project_title(self):
        data = {
            "title": "Patched"
        }
        url = f"{self.projects_url}2/"
        response = self.client.patch(url, data, format='json')
        project = Project.objects.get(pk=2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], project.title)

    def test_patch_project_staff(self):
        data = {
            "staff": []
        }
        url = f"{self.projects_url}2/"
        response = self.client.patch(url, data, format='json')
        project = Project.objects.get(pk=2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(project.staff.exists())

    def test_patch_project_without_creds(self):
        data = {
            "staff": []
        }
        self.client.credentials()
        url = f"{self.projects_url}2/"
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_patch_project_as_developer(self):
        data = {
            "staff": []
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token ' +
                                self.token_developer.key)
        url = f"{self.projects_url}2/"
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_put_project_staff(self):
        data = {
            "title": "putted"
        }
        url = f"{self.projects_url}2/"
        response = self.client.put(url, data, format='json')
        project = Project.objects.get(pk=2)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(project.title, data['title'])

    def test_get_tasks(self):
        fields = ('id', 'title', 'description', 'created', 'due_date',
                  'project', 'assigned_to', 'hour_notificated',
                  'day_notificated')
        response = self.client.get(self.tasks_url, format='json')
        data = json.loads(response.content)
        task_count = Project.objects.count()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(all([field in data[0] for field in fields]))
        self.assertEqual(task_count, len(data))

    def test_post_task(self):
        data = {
            "title": "third",
            "description": "imagination",
            "due_date": self.day_after_tommorow,
        }
        response = self.client.post(self.tasks_url, data, format='json')
        count_tasks = Task.objects.count()
        last_task = Task.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(count_tasks, 3)
        self.assertEqual(response.data['title'], last_task.title)
        self.assertEqual(last_task.creator, self.test_manag)

    def test_post_task_less_then_day(self):
        data = {
            "title": "third",
            "description": "imagination",
            "due_date": self.now_plus_hour,
        }
        response = self.client.post(self.tasks_url, data, format='json')
        last_task = Task.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(last_task.day_notificated)

    def test_post_task_no_creds(self):
        data = {
            "title": "third",
            "description": "imagination",
            "due_date": self.day_after_tommorow,
        }
        self.client.credentials()
        response = self.client.post(self.tasks_url, data, format='json')
        count_tasks = Task.objects.count()
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(count_tasks, 2)

    def test_post_task_as_developer(self):
        data = {
            "title": "third",
            "description": "imagination",
            "due_date": self.day_after_tommorow,
        }
        self.client.credentials(HTTP_AUTHORIZATION='Token ' +
                                self.token_developer.key)
        response = self.client.post(self.tasks_url, data, format='json')
        count_tasks = Task.objects.count()
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(count_tasks, 2)

    def test_post_task_expired_date(self):
        data = {
            "title": "third",
            "description": "imagination",
            "due_date": self.expired_date,
        }
        response = self.client.post(self.tasks_url, data, format='json')
        last_task = Task.objects.last()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(last_task.day_notificated)
        self.assertTrue(last_task.hour_notificated)
    
    def test_put_task(self):
        data = {
            "title": "putted",
            "description": "",
            "due_date": self.now_plus_hour,
        }
        url = f"{self.tasks_url}1/"
        response = self.client.put(url, data, format='json')
        task = Task.objects.get(pk=1)
        tasks_count = Task.objects.count()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(tasks_count, 2)
        self.assertEqual(task.title, data['title'])
        self.assertTrue(task.day_notificated)
        self.assertFalse(task.hour_notificated)

    def test_put_task_without_due_date(self):
        data = {
            "title": "putted",
            "description": ""
        }
        url = f"{self.tasks_url}1/"
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_patch_task_without_due_date(self):
        data = {
            "title": "patched",
            "description": ""
        }
        url = f"{self.tasks_url}1/"
        response = self.client.patch(url, data, format='json')
        task = Task.objects.get(pk=1)
        tasks_count = Task.objects.count()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(tasks_count, 2)
        self.assertEqual(task.title, data['title'])
        self.assertFalse(task.day_notificated)
        self.assertFalse(task.hour_notificated)

    def test_patch_task_due_date(self):
        data = {
            "due_date": self.expired_date
        }
        url = f"{self.tasks_url}1/"
        response = self.client.patch(url, data, format='json')
        task = Task.objects.get(pk=1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(task.day_notificated)
        self.assertTrue(task.hour_notificated)
