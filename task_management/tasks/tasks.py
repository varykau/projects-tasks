import datetime as dt

from django.conf import settings

from task_management.celery import app
from .models import Task
from services.mail_sender import send_notification

HOUR_NOTIFICATION = '1h'


def check_notifications(period, tasks_set):
    now = dt.datetime.now()
    time_interval = now + settings.PERIODS[period][1]
    for task in tasks_set:
        if time_interval >= task.due_date.replace(tzinfo=None):
            send_notification(task, task.assigned_to.email, period)
            setattr(task, settings.PERIODS[period][2], True)
            if period == HOUR_NOTIFICATION and not task.day_notificated:
                task.day_notificated = True
            task.save()


def pre_hour_notify():
    tasks = Task.objects.filter(hour_notificated=False,
                                assigned_to__isnull=False
                                )
    check_notifications(HOUR_NOTIFICATION, tasks)


def pre_day_notify():
    one_day = '1d'
    tasks = Task.objects.filter(day_notificated=False,
                                assigned_to__isnull=False
                                )
    check_notifications(one_day, tasks)


@app.task(ignore_result=True)
def one_hour_notificate():
    pre_hour_notify()


@app.task(ignore_result=True)
def one_day_notificate():
    pre_day_notify()
