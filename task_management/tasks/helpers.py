import datetime as dt

from django.utils import timezone
from rest_framework import serializers


class NotificationChecker:
    def __init__(self, task_validated):
        self._task = task_validated
        self._due_date = self.__get_due_date()

    def __get_due_date(self):
        return self._task.get('due_date')

    def check_future_notifications(self):
        self.__check_expired_task()

    def check_expired_task(self):
        if self._due_date < timezone.now():
            self._task['day_notificated'] = True
            self._task['hour_notificated'] = True

    def check_notification_early_then_one_day(self):
        one_day = dt.timedelta(days=1)
        if (timezone.now() + one_day) > self._due_date:
            self._task['day_notificated'] = True
    
    def __set_notifications_to_false(self):
        self._task['day_notificated'] = False
        self._task['hour_notificated'] = False

    def full_check_notification(self):
        if self._due_date:
            self.__set_notifications_to_false()
            self.check_expired_task()
            self.check_notification_early_then_one_day()
        return self._task
