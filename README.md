Для запуска проекта необходимо создать файл local_settings.py в папке ./task_management/task_management/
Рядом лежит файл local_settings.txt, который указывает названия переменных.

Также в папке ./task_management нужно создать файл .env, который хранит названия переменных. Пример названия переменных
лежит в файле env-examples.

После создания файла local_settings.py выполнить в консоли команду из корневой директории docker-compose up --build.


urls:
    - api/v1/users
    - api/v1/projects
    - api/v1/tasks
    - api/v1/api-token - POST-запрос на получение токена
